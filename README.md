# Simple Java application using Maven


[![pipeline status](https://gitlab.com/yash.mangukiya/sample-maven-pipeline/badges/main/pipeline.svg)](https://gitlab.com/yash.mangukiya/sample-maven-pipeline/-/commits/main)


[![coverage report](https://gitlab.com/yash.mangukiya/sample-maven-pipeline/badges/main/coverage.svg)](https://gitlab.com/yash.mangukiya/sample-maven-pipeline/-/commits/main)

This code is part of a blog post.

The link to the blog post is given here.
[https://adityasridhar.com/posts/how-to-get-started-with-maven](https://adityasridhar.com/posts/how-to-get-started-with-maven)

Read the blog post to make the best use of this repo :)

## Pre-requisite

Ensure you have maven installed in your system. You can install it from [https://maven.apache.org/](https://maven.apache.org/)

Also ensure maven path is set in you System so that you can run `mvn` commands

## Cloning the code to your local

Clone this code to your local using the following command

```bash
git clone https://github.com/aditya-sridhar/first-maven-app.git
```

Or you can import the project into an IDE of your choice.
